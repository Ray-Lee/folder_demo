class AddAncestryDepthToFolder < ActiveRecord::Migration[5.0]
  def change
    add_column :folders, :ancestry_depth, :integer, :default => 0
  end
end
