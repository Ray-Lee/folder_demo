class CreateAccessibilities < ActiveRecord::Migration[5.0]
  def change
    create_table :accessibilities do |t|
      t.integer :owner_id
      t.integer :folder_id
      t.integer :user_id
      t.boolean :can_read, default: false
      t.boolean :can_write, default: false

      t.timestamps
      t.index [:owner_id, :folder_id, :user_id], name: "index_accessibilities_on_owner_folder_user_ids", unique: true
    end
  end
end
