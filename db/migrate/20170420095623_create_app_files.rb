class CreateAppFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :app_files do |t|
      t.string :name
      t.integer :user_id
      t.integer :folder_id
      t.string :temp_path

      t.timestamps
    end
  end
end
