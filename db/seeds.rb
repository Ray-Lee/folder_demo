# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.delete_all
AppFile.delete_all
Folder.delete_all

u = User.create
u.app_files.create(name: 'Gemfile', temp_path: 'folder_demo')
u.app_files.create(name: 'Gemfile.lock', temp_path: 'folder_demo')
u.app_files.create(name: 'config.ru', temp_path: 'folder_demo')
u.app_files.create(name: 'README.md', temp_path: 'folder_demo')

u.app_files.create(name: 'app_file.rb', temp_path: 'folder_demo/app/models')
u.app_files.create(name: 'folder.rb', temp_path: 'folder_demo/app/models')
u.app_files.create(name: 'user.rb', temp_path: 'folder_demo/app/models')
u.app_files.create(name: '.keep', temp_path: 'folder_demo/app/models/concerns')

u.app_files.create(name: 'users_controller.rb', temp_path: 'folder_demo/app/controllers')
u.app_files.create(name: 'folders_controller.rb', temp_path: 'folder_demo/app/controllers')

u.app_files.create(name: 'index.html.rb', temp_path: 'folder_demo/app/views/folders')
u.app_files.create(name: 'new.html.rb', temp_path: 'folder_demo/app/views/folders')
u.app_files.create(name: 'show.html.rb', temp_path: 'folder_demo/app/views/folders')
u.app_files.create(name: 'edit.html.rb', temp_path: 'folder_demo/app/views/folders')

u.app_files.create(name: 'index.html.rb', temp_path: 'folder_demo/app/views/users')
u.app_files.create(name: 'new.html.rb', temp_path: 'folder_demo/app/views/users')
u.app_files.create(name: 'show.html.rb', temp_path: 'folder_demo/app/views/users')
u.app_files.create(name: 'edit.html.rb', temp_path: 'folder_demo/app/views/users')

u.app_files.create(name: 'database.yml', temp_path: 'folder_demo/config')
u.app_files.create(name: 'route.rb', temp_path: 'folder_demo/config')
u.app_files.create(name: 'environment.rb', temp_path: 'folder_demo/config')
u.app_files.create(name: 'development.rb', temp_path: 'folder_demo/config/environments')

u.app_files.create(name: 'seeds.rb', temp_path: 'folder_demo/db')
u.app_files.create(name: 'schema.rb', temp_path: 'folder_demo/db')
