# About
1. 關於 Data Center, NL資料夾功能[短期解]至[長期解]的 migration demo
2. Folder move/delete demo
3. Folder sharing demo

# Model
1. User, 使用者
2. AppFile, 使用者上傳的檔案(ex: nl 筆記, pdf)
3. Folder, 樹狀結構資料夾
4. Accessibility, 資料夾的存取權限

# Key Notes
1. AppFile#temp_path, 短期儲存NL資料夾名稱的欄位
2. FolderConverter.run, 為所有 temp_path 有值的 AppFile 建立並設定對應 Folder
3. Folder#full_path, return string of names from root folder to self with / seprate, ex: folder_demo/app/views/folders
4. AppFile#full_path, return string of names from root folder to self with / seprate, ex: folder_demo/app/views/folders/index.html.rb
5. User#allowances, 列出已建立的存取權限
6. User#accessibilities, 列出被授權的存取權限
7. Folder#can_user_read?(user_id), 回傳user是否被授權讀取
8. Folder#can_user_write?(user_id), 回傳user是否被授權寫入

# Nice Helper
https://github.com/stefankroes/ancestry#navigating-your-tree

# Setup Initial Data
    rake db:migrate
    rake db:seed
