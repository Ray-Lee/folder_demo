class Accessibility < ApplicationRecord
  belongs_to :owner, class_name: :User, foreign_key: :owner_id
  belongs_to :user
  belongs_to :folder

  before_save :check_owner

  private
  def check_owner
    raise 'forbidden: not folder owner' unless self.folder.owner_id?(self.owner.id)
  end

end
