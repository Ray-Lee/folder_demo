class User < ApplicationRecord
  has_many :app_files, dependent: :destroy
  has_many :folders, dependent: :destroy
  has_many :allowances, class_name: :Accessibility, foreign_key: :owner_id, dependent: :destroy
  has_many :accessibilities, dependent: :destroy
end
