class AppFile < ApplicationRecord
  belongs_to :user
  belongs_to :folder

  def full_path
    self.folder.full_path + '/' + self.name
  end
end
