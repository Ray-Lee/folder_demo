class Folder < ApplicationRecord
  belongs_to :user
  has_many :app_files, dependent: :destroy
  has_many :accessibilities, dependent: :destroy

  has_ancestry cache_depth: true, touch: true

  before_save :inherit_user

  def full_path
    self.path.map(&:name).join('/')
  end

  def move_into(folder_id)
    target_folder = self.user.folders.find_by(id: folder_id)
    return nil if target_folder.nil?

    folders_to_move = self.children.to_a
    folders_to_move << self

    new_path = target_folder.ancestry + '/' + target_folder.id.to_s
    old_path = self.ancestry

    folders_to_move.each do |folder|
      folder.ancestry = folder.ancestry.sub(old_path, new_path)
      folder.save!
    end

    return folders_to_move
  end

  def can_user_read?(user_id)
    return true if owner_id?(user_id)
    acc = get_accessibility_to_apply(user_id)
    return acc.nil? ? false : acc.can_read
  end

  def can_user_write?(user_id)
    return true if owner_id?(user_id)
    acc = get_accessibility_to_apply(user_id)
    return acc.nil? ? false : acc.can_write
  end

  def owner_id?(user_id)
    return self.user.id == user_id
  end

  private
  def inherit_user
    self.user = self.root.user if self.user.nil?
  end

  def get_accessibility_to_apply(user_id)
    accessibility_to_apply = nil
    acc = :accessibilities
    folders = self.path.includes(acc).where(acc => { user_id: user_id })
    return folders.last.accessibilities.first
  end
end
