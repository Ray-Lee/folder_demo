class FolderConverter

  def self.run
    tartget_app_files = AppFile.where.not(temp_path: nil)
    puts "[#{Time.now}] target count: #{tartget_app_files.count}"

    tartget_app_files.each_with_index do |app_file, progress_count|
      next unless is_valid?(app_file.temp_path)
      temp_folders = app_file.temp_path.split('/')
      folder_ids = []
      temp_folders.each_with_index do |temp_folder, index|
        ancestry_string = (index == 0) ? nil : folder_ids.map(&:inspect).join('/')
        folder_ids << app_file.user.folders.find_or_create_by(name: temp_folder, user: app_file.user, ancestry: ancestry_string).id
      end
      app_file.update(folder_id: folder_ids.last)
      puts "[#{Time.now}] count: #{progress_count}" if (progress_count % 100 == 0)
    end
    tartget_app_files.reload
  end

  private
  def self.is_valid?(path)
    return true
  end

end
